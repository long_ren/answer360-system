<?php

function smarty_modifier_truncate_j($string, $length = 80, $etc = '...',
                                  $break_words = false, $middle = false)
{
    //进行判断如果，字符长度大于用户输入的长度，证明有多，RETURN加了....的，如果小于，证明没有进行截取，不用加....
    if(mb_strlen($string,'utf8')>$length){
        return mb_substr($string, 0,$length,'utf8').$etc;
    }else{
        return mb_substr($string, 0,$length,'utf8');
    }

}



?>
