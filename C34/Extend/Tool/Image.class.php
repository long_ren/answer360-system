<?php
/* 
* [图像处理类]
* @author: mazhenyu[houdunwangmzy@163.com]
* @Date:   2014-05-20 17:24:59
* @Last Modified time: 2014-05-21 15:59:58
*/
class Image{
	//缩略图属性
	private $thumbW;
	private $thumbH;
	private $prefix;

	//水印属性
	private $position;
	private $pct;

	public function thumb($img,$path = NULL,$thumbW = NULL,$thumbH = NULL,$prefix = NULL){
		//初始化配置
		$this->thumbW = is_null($thumbW) ? C('THUMB_WIDTH') : $thumbW;
		$this->thumbH = is_null($thumbH) ? C('THUMB_HEIGHT') : $thumbH;
		$this->prefix = is_null($prefix) ? C('THUMB_PREFIX') : $prefix;

		//源图
		//获得源图信息
		$srcInfo = getimagesize($img);
		$srcW = $srcInfo[0];
		$srcH = $srcInfo[1];

		//源图类型
		$srcType = ltrim(strrchr($srcInfo['mime'], '/'),'/');

		//打开源图
		$fn = "imagecreatefrom" . $srcType;
		$srcImg = $fn($img);

		//计算比例
		$ratioW = $this->thumbW / $srcW;
		$ratioH = $this->thumbH / $srcH;

		$ratio = max($ratioW,$ratioH);

		//用比例计算目标图的宽高
		$thumbW = $this->thumbW * $ratio;
		$thumbH = $this->thumbH * $ratio;

		//目标图（画布）
		$dstImg = imagecreatetruecolor($thumbW, $thumbH);
		//缩放源图到目标图
		if(function_exists('imagecopyresampled')){
			imagecopyresampled($dstImg, $srcImg, 0, 0, 0, 0, $thumbW, $thumbH, $srcW, $srcH);
		}else{
			imagecopyresized($dstImg, $srcImg, 0, 0, 0, 0, $thumbW, $thumbH, $srcW, $srcH);
		}
		
		//处理保存路径
		$path = is_null($path) ? dirname($img) : $path;
		$path = rtrim(str_replace('\\', '/', $path),'/') . '/';

		is_dir($path) || mkdir($path, 0777, true);

		//保存缩略图
		$fn = "image" . $srcType;
		//完整路径带有文件名
		$fullPath = $path . $this->prefix . basename($img); 

		//保存
		$fn($dstImg,$fullPath);
		//销毁
		imagedestroy($srcImg);
		imagedestroy($dstImg);

		//返回缩略图完整路径带有文件名
		return $fullPath;
	}

	/**
	 * [water 水印]
	 * @param  [type] $img      [description]
	 * @param  [type] $water    [description]
	 * @param  [type] $position [description]
	 * @param  [type] $opacity  [description]
	 * @return [type]           [description]
	 */
	public function water($img,$water,$position = NULL, $pct = NULL){
		$this->position = is_null($position) ? C('WATER_POSITION') : $position;
		$this->pct = is_null($pct) ? C('WATER_PCT') : $pct;

		//目标图（大图）*******************
		$dstInfo = getimagesize($img);
		//类型
		$dstType = ltrim(strrchr($dstInfo['mime'], '/'),'/');

		//目标图宽高
		$dstW = $dstInfo[0];
		$dstH = $dstInfo[1];


		//打开目标图
		$fn = "imagecreatefrom" . $dstType;
		$dstImg = $fn($img);

		//源图（水印）*******************
		$srcInfo = getimagesize($water);
		//类型
		$srcType = ltrim(strrchr($srcInfo['mime'], '/'),'/');

		//打开源图
		$fn = "imagecreatefrom" . $srcType;
		$srcImg = $fn($water);

		//源图宽高
		$srcW = $srcInfo[0];
		$srcH = $srcInfo[1];

		//计算目标图坐标
		$arr = $this->_get_position($dstW,$dstH,$srcW,$srcH);

		$x = $arr['x'];
		$y = $arr['y'];

		//合并两张图片
		if($srcType == 'png' || $srcType == 'gif'){
			imagecopy($dstImg, $srcImg, $x, $y, 0, 0, $srcW, $srcH);
		}else{
			imagecopymerge($dstImg, $srcImg, $x, $y, 0, 0, $srcW, $srcH, $this->pct);
		}
			
		//保存水印
		$path = dirname($img);
		$path = rtrim(str_replace('\\', '/', $path),'/') . '/';

		$fileName = time() . mt_rand(0,9999) . '.' . $dstType;
		//组合完整路径
		$fullPath = $path . $fileName;

		$fn = "image" . $dstType;
		$fn($dstImg,$fullPath);

		//销毁
		imagedestroy($dstImg);
		imagedestroy($srcImg);
	}

	/**
	 * [get_position 计算水印位置]
	 * @param  [type] $width  [description]
	 * @param  [type] $height [description]
	 * @return [type]         [description]
	 */
	private function _get_position($dstW,$dstH,$srcW,$srcH){

		$arr['x'] = 20;
		$arr['y'] = 20;

		switch ($this->position) {
			case 1:
				break;

			case 2:
				$arr['x'] = ($dstW - $srcW) / 2 + 20;
				break;
			case 9:
				$arr['x'] = $dstW - $srcW - 20;
				$arr['y'] = $dstH - $srcH - 20;
				break;
			
			default:
				$arr['x'] = mt_rand(0,$dstW - $srcW);
				$arr['y'] = mt_rand(0,$dstH - $srcH);
				break;
		}

		return $arr;
	}
}





?>
