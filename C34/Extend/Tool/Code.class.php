<?php  
	//****************生成验证码方法****************
	class Code{
		//定义验证码图片
		private $img;
		//定义验证码宽度
		private $width;
		//定义验证码高度
		private $height;
		//定义验证码字符长度
		private $codeLen;
		//定义验证码字体大小
		private $fontSize;
		//定义验证码背景颜色
		private $bgColor;
		//定义验证码字符
		private $seed;
		//定义验证码字体
		private $fontFile;
		public $str;

		//****************构造函数 将配置项中的值传进来****************
		public function __construct($width = NULL, $height = NULL, $codeLen = NULL, $fontSize = NULL, $bgColor = NULL,$fontColor = NULL){
			//获得验证码的宽度
			$this->width = is_null($width) ? C('CODE_WIDTH') : $width;
			//获得验证码的高度
			$this->height = is_null($height) ? C('CODE_HEIGHT') : $height;
			//获得验证码的字符长度
			$this->codeLen = is_null($codeLen) ? C('CODE_LEN') : $codeLen;
			//获得验证码的字体大小
			$this->fontSize = is_null($fontSize) ? C('CODE_FONTSIZE') : $fontSize;
			//获得验证码的背景颜色
			$this->bgColor = is_null($bgColor) ? C('CODE_BGCOLOR') : $bgColor;
			//获得验证码的字体
			$this->fontFile = C('CODE_FILE');
			//获得验证码的字符
			$this->seed = C('CODE_SEED');
		}

		//****************定义验证码显示方法****************
		public function show(){
			//发送图像头部
			ob_clean();
			header('Content-type:image/png');
			//创建画布 填充颜色
			$this->_create_bg();
			//写入字符
			$this->_create_font();
			//创建干扰线
			$this->_create_line();
			//创建干扰点
			$this->_create_point();
			//输出验证码
			imagepng($this->img);
			//关闭销毁验证码
			imagedestroy($this->img);
		}

		//****************定义创建背景方法****************
		private function _create_bg(){
			//创建画布
			$img = imagecreatetruecolor($this->width, $this->height);
			//转换颜色 将16进制转换为10进制
			$color = hexdec($this->bgColor);
			//填充颜色
			imagefill($img, 0, 0, $color);
			//属性赋值
			$this->img = $img;
		}

		//****************定义创建字符方法****************
		private function _create_font(){
			//设置验证码字符长度
			$seedLen = strlen($this->seed);
			//定义空字符串
			$str = '';
			//循环组合验证码
			for ($i=0; $i < $this->codeLen; $i++) {
				//创建随机字体颜色
				$color = imagecolorallocate($this->img, mt_rand(0,255), mt_rand(0,255), mt_rand(0,255));
				//获得要写入的字体
				$text = $this->seed[mt_rand(0,$seedLen - 1)];
				//连接字符串
				$str .= $text;
				//计算坐标
				$x = ($this->width / $this->codeLen) * $i + 10;
				$y = ($this->height + $this->fontSize) / 2;
				//写入字符
				imagettftext($this->img, $this->fontSize, mt_rand(-20,20), $x, $y, $color, $this->fontFile, $text);
			}
			$_SESSION['code'] = $str;
		}

		//****************定义创建干扰点方法****************
		private function _create_point(){
			//循环创建干扰点
			for ($i=0; $i < 50; $i++) { 
				//设置随机颜色
				$color = imagecolorallocate($this->img, mt_rand(0,255), mt_rand(0,255), mt_rand(0,255));
				//创建干扰点
				imagesetpixel($this->img, mt_rand(0,$this->width), mt_rand(0,$this->height), $color);
			}
		}

		//****************定义创建干扰线方法****************
		private function _create_line(){
			//循环创建干扰线
			for ($i=0; $i < 5; $i++) { 
				//设置随机颜色
				$color = imagecolorallocate($this->img, mt_rand(0,255), mt_rand(0,255), mt_rand(0,255));
				//创建干扰线
				imageline($this->img, mt_rand(0,$this->width), mt_rand(0,$this->height), mt_rand(0,$this->width), mt_rand(0,$this->height), $color);
			}
		}		
	}
?>