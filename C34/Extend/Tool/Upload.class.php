<?php
/* 
* [***********************]
* @author: mazhenyu[houdunwangmzy@163.com]
* @Date:   2014-05-16 17:14:17
* @Last Modified time: 2014-05-19 16:04:34
*/

//1.重组数组
//2.过滤不合法的
//3.上传移动
//4.返回上传信息

class Upload{
	//允许类型
	private $type;
	//允许大小
	private $size;
	//上传路径
	private $path;

	//外部可以获得的错误
	public $error;


	/**
	 * [__construct 构造函数，初始化配置]
	 * @param [type] $type [description]
	 * @param [type] $size [description]
	 */
	public function __construct($type = NULL,$size = NULL,$path = NULL){
		//允许类型
		$this->type = is_null($type) ? C('UPLOAD_TYPE') : $type;
		//允许大小
		$this->size = is_null($size) ? C('UPLOAD_SIZE') : $size;
		//上传路径
		$this->path = is_null($path) ? C('UPLOAD_FILE') : $path;
		
	}

	/**
	 * [up 提供给外部上传的方法]
	 * @return [type] [description]
	 */
	public function up(){

		// //1.重组数组
		$arr = $this->_reset_arr();
		//保存上传以后的信息
		$infoArr = array();
		//筛掉不符合的图片
		foreach ($arr as $v) {
			//如果成功，则移动上传文件
			if($this->_filter($v)){
				//移动上传
				$infoArr[] = $this->_move($v);
			}
		}
		//返回包含上传以后的路径和文件名信息数组还包含以前的（name,type,error,等一些信息）
		return $infoArr;
	}

	/**
	 * [_reset_arr 重组数组]
	 * @return [type] [description]
	 */
	private function _reset_arr(){
		$tempArr = array();
		foreach ($_FILES as $v) {
			if(is_array($v['name'])){
				foreach ($v['name'] as $key => $value) {
					$tempArr[] = array(
						'name'		=>	$value,
						'type'		=>	$v['type'][$key],
						'tmp_name'	=>	$v['tmp_name'][$key],
						'error'		=>	$v['error'][$key],
						'size'		=>	$v['size'][$key]
						);
				}	
			}else{
				$tempArr[] = $v;
			}
			
		}
		return $tempArr;
	}

	/**
	 * [_filter 过滤]
	 * @param  [type] $v [description]
	 * @return [type]    [description]
	 */
	private function _filter($v){
		//1.判断是不是上传文件
		//2.类型是否允许
		//3.大小是否允许
		//4.error错误代码

		//获得类型
		$type = pathinfo($v['name']);
		$type = isset($type['extension']) ? $type['extension'] : '';


		switch (true) {
			case !is_uploaded_file($v['tmp_name']):
				$this->error = '不是一个合法上传文件';
				return false;

			case !in_array($type, $this->type):
				$this->error = '上传类型不允许';
				return false;

			case $v['size'] > $this->size:
				$this->error = '超过网站配置项大小';
				return false;
			
			case $v['error'] == 1:
				$this->error = '大小超过了 php.ini 中 upload_max_filesize 限制值';
				return false;

			case $v['error'] == 2:
				$this->error = '大小超过 HTML 表单中 MAX_FILE_SIZE 选项指定的值';
				return false;

			case $v['error'] == 3:
				$this->error = '文件只有部分被上传';
				return false;

			case $v['error'] == 4:
				$this->error = '没有文件被上传';
				return false;

			default:
				return true;
		}
	}

	/**
	 * [_move 移动上传]
	 * @param  [type] $v [description]
	 * @return [type]    [description]
	 */
	private function _move($v){
		//获得类型
		$type = pathinfo($v['name']);
		$type = isset($type['extension']) ? $type['extension'] : '';

		$fileName = time() . mt_rand(0,9999) . '.' . $type;

		$path = rtrim(str_replace('\\', '/', $this->path),'/') . '/';

		is_dir($path) || mkdir($path, 0777, true);

		$fullPath = $path . $fileName;

		move_uploaded_file($v['tmp_name'], $fullPath);

		//返回保存路径
		$v['savePath'] = $fullPath;
		return $v;
	}
}
?>
