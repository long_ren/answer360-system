<?php 
class Model{
	// 静态方法
	static private $link =NULL;
	//当前表属性
	private $table;
	private $opt = array();
//构造函数，初始化数据
public function __construct($table){
	//连接数据库
	$this->_connect();
	// //设置表名
	$this->table =C('DB_PREFIX').$table;
	$this->opt();
}

private function _connect(){
	//连接数据库《调用配置项里面的数据库等信息》
	$link = @new Mysqli(C('DB_HOST'),C('DB_USER'),C('DB_PASSWORD'),C('DB_NAME'));
	if($link->connect_errno) halt('数据库连接错误或者选择库失败');
	//将字符集付给一个变量方便后面的处理<安全处理，防止数据库注入>
	$charset = 'SET CHARACTER_SET_CLIENT=BINARY,CHARACTER_SET_CONNECTION='.C('DB_CHARSET').',CHARACTER_SET_RESULTS='.C('DB_CHARSET');
	//设置字符集
	$link ->query($charset);
	//把连接信息保存到静态属性里面
	self::$link=$link;
}

// add************************************************** 
		//默认为空，为的是为便POST提交过来的数据，如果数组没有传值的话，就执行POST提交过来的数据；
public function add($arr = NULL){
		// 如果数组为空，那数据就为POST接过来的数据
	if(is_null($arr)) $arr = $_POST;
	//取得数组键名的值，并且转为字符串
	$strKeys = implode(',', array_keys($arr));
	//取得数据键值的值，并且转为字符串
	$strValues ="'" . implode("','", array_values($arr)) . "'";
	//拼合SQL语句
	$sql ="INSERT INTO " . $this->table ." (" . $strKeys . ")" . " VALUES (".$strValues  . ")";
	// 执行结果集
	return $this->exe($sql);

}
// delete************************************************** 
public function delete(){
	//为了防止把数据库里面的数据库全部删除，对WHERE进行为空判断如果为空，不让它进行后面的操作
	if(empty($this->opt['where'])) halt('删除语句必须拥有WHERE条件');
	//组合SQL语句
	$sql ="DELETE FROM ".$this->table .$this->opt['where'];
	// 执行无结果集类
	return $this->exe($sql);


}
// upadte************************************************** 
public function update($arr=NULL){
	//为了防止把数据库里面的数据库全部修改，对WHERE进行为空判断如果为空，不让它进行后面的操作
	if(empty($this->opt['where'])) halt('修改语句必须拥有WHERE条件');
	//如果数组为空，就执行POST提入的语句
	if(is_null($arr)) $arr = $_POST;
	// 新建一个空的字符串，方便组合SQL语句
	$var ='';
	// 循环接把数据转成字符串
	foreach ($arr as $k => $v) {
		$var .= $k . "='" . $v ."',"; 
	}
	//因为组合出来的字符串多一个逗号，用RTRIM剪掉
	$var = rtrim($var,',');
	//组合SQL语句
	$sql = "UPDATE " . $this->table . " SET " . $var .$this->opt['where'];
		echo $sql;
	// 执行无结果集类
 	return $this->exe($sql);
	


}
// opt************************************************** 
	//选择条件类，默认为*
public function opt(){
	$this->opt = array(
		'field' => '*',
		'where' =>	'',
		'group' =>	'',
		'having'=>	'',
		'order' =>	'',
		'limit'	=>	''
		);
	

}

//field ************************************************** 
//选择条件类，返回对象方便键式操作
public function field ($field){
	$this->opt['filed']=$field;
	return $this;
}

//where **************************************************
//选择条件类，返回对象方便键式操作 
public function where ($where){
	$this->opt['where']=' WHERE ' . $where;
	return $this;
}
//group ************************************************** 
public function group($group){
	$this->opt['group']=' GROUP ' . $group;
	return $this;
}
//having ************************************************** 
public function having($having){
	$this->opt['having']= ' HAVING ' . $having;
	return $this;
}

//order **************************************************
public function order($order){
	$this->opt['order']= ' ORDER BY ' . $order;
	return $this;
} 

//limit ************************************************** 
public function limit($limit){
	$this->opt['limit']= ' LIMIT ' . $limit;
	return $this;
}
//finAall<别名> **************************************************
public function findAll(){
	return $this->all();
}  
//limit ************************************************** 
public function find(){
	$this->opt['limit'] = ' LIMIT 1';
	$data = $this->all();
	return current($data);
}
//select<别名> **************************************************  
public function select(){
	return $this->all();
}
//执行结果集 ************************************************** 
public function query($sql){
	// 执行QUERY
	$result =self::$link->query($sql);
	//对SQL语句进行判断，如果出错，就会出现出错提示
	if(self::$link->errno){
		halt(self::$link->error.'<h2 style="color:red;">SQl:'.$sql.'</h2>');
	}
	// 新建空数组，方便后面的数据压入
	$rows = array();
	//循环获得数据
	while ($row = $result->fetch_assoc()) {
		// 将数据压入数组
		$rows[]= $row;

	}
	// free结果集,释放内存
	$result->free();
	
	//return出数据
	return $rows;
} 
//执行无结果集 ************************************************** 
public function exe($sql){
	// 执行QUERRY
	 self::$link->query($sql);
	// 返出处理后的值<三元表达式，无结果集处理有插入数据和修改数据，插入数库返回的是自增ID，但是修改数据返回的是所变化的结果行数。两个返回的值不一样。所以用个三元表达式判断下。> 
	 return self::$link->insert_id ? self::$link->insert_id :self::$link->affected_rows;
 


	}
//all **************************************************
//查询数据
public function all(){
	// 组合SQL语句
	$sql = "SELECT " . $this->opt['field'] . " FROM " . $this->table .$this->opt['where'].$this->opt['group'] . $this->opt['having'] . $this->opt['order'] . $this->opt['limit'];
	// 返出查询结果
	return $this->query($sql);

}

//析构函数，执行完类后-》执行
public function __destruct(){
		// 关闭数据库,节省资源
		self::$link->close();
		//刚$LINK值清空为NULL
		self::$link = NULL;
	}



}


 ?>
