<?php
/* 
* [***********************]
* @author: mazhenyu[houdunwangmzy@163.com]
* @Date:   2014-05-22 15:48:14
* @Last Modified time: 2014-05-26 16:07:06
*/
class C34{

	public static function run(){
		//设置框架所需常量
		self::_set_const();
		//创建用户应用文件夹
		self::_create_dir();
		//载入框架所需文件
		self::_import_file();
		//执行应用
		Application::run();
	}


	/**
	 * [_set_const 定义框架所需常量]
	 */
	private static function _set_const(){
		// echo __FILE__;
		//定义框架路径
		$path = str_replace('\\', '/', __FILE__);
		define('C34_PATH', dirname($path));
		//定义项目根路径
		define('ROOT_PATH', dirname(C34_PATH));
		// echo C34_PATH;
		// 框架库路径
		define('LIB_PATH', C34_PATH . '/Lib');
		// 框架编译路径
		define('COMPILE_PATH', LIB_PATH . '/Compile');
		// 框架核心文件
		define('CORE_PATH', LIB_PATH . '/Core');
		// 框架函数路径
		define('FUNCTION_PATH', LIB_PATH . '/Function');
		// 定义扩展Extend路径
		define('EXTEND_PATH', C34_PATH . '/Extend');
		// 定义Tool路径
		define('TOOL_PATH', EXTEND_PATH . '/Tool');
		// 定义Data 路径
		define('DATA_PATH', C34_PATH . '/Data');
		// 定义Font 字体文件路径
		define('FONT_PATH', DATA_PATH . '/Font');
		

		//设置用户应用常量
		define('APP_PATH', dirname(C34_PATH) . '/' . APP_NAME);
		//用户控制器文件夹
		define('APP_CONTROL_PATH', APP_PATH . '/Control');
		//用户配置项文件夹
		define('APP_CONFIG_PATH', APP_PATH . '/Config');
		//用户模板文件夹
		define('APP_TPL_PATH', APP_PATH . '/Tpl');
		//定义临时文件夹
		define('APP_TEMP_PATH', APP_PATH . '/Temp');
		//定义编译文件夹
		define('APP_COMPILE_PATH', APP_TEMP_PATH .'/Compile');
		//定义缓存文件夹
		define('APP_CACHE_PATH', APP_TEMP_PATH . '/Cache');
		
		//用户模板文件夹里面的public,放css,js,images
		define('APP_PUBLIC_PATH', APP_TPL_PATH . '/Public');
		//是否是POST提交
		define('IS_POST', ($_SERVER['REQUEST_METHOD'] == 'POST') ? TRUE : FALSE);
		//是否是AJAX提交
		if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'){
			define('IS_AJAX', TRUE);
		}else{
			define('IS_AJAX', FALSE);
		}
		
	}

	/**
	 * [_create_dir 创建应用项目文件夹]
	 * @return [type] [description]
	 */
	private static function _create_dir(){
		$arr = array(
			APP_PATH,
			APP_CONTROL_PATH,
			APP_CONFIG_PATH,
			APP_TPL_PATH,
			APP_PUBLIC_PATH,
			APP_COMPILE_PATH,
			APP_CACHE_PATH
			);

		foreach ($arr as $v) {
			is_dir($v) || mkdir($v, 0777, true);
		}
	}

	/**
	 * [_import_file 载入框架所需文件]
	 * @return [type] [description]
	 */
	private static function _import_file(){
		//编译文件,里面包含框架所需文件
		$arr = include COMPILE_PATH . '/compile.php';
		//循环载入
		foreach ($arr as $v) {
			require_once $v;
		}
	}
}


C34::run();











