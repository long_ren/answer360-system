<?php
/* 
* [***********************]
* @author: mazhenyu[houdunwangmzy@163.com]
* @Date:   2014-05-22 16:25:49
* @Last Modified time: 2014-05-22 17:38:03
*/
return array(
	FUNCTION_PATH . '/function.php',
	EXTEND_PATH.'/Org/Smarty/Smarty.class.php',
	CORE_PATH .'/SmartyView.class.php',
	CORE_PATH . '/Control.class.php',
	CORE_PATH . '/Application.class.php',
	);
?>
