<?php 
class SmartyView{
private static $smarty = NULL;
// 构造配置项*************************************
public function __construct(){
	//self::$smarty为真，就不为空，就不用走构造函数的配置项，直接RETURN出去
	if(self::$smarty) return;
	//构造函数，先载入SMARTY的相关配置
	$smarty = new Smarty();
	// 模板目录
	$smarty->template_dir = APP_TPL_PATH . '/' . CONTROL ;
	
	//编译目录
	$smarty->compile_dir = APP_COMPILE_PATH;
	//开始定界符
	$smarty->left_delimiter = C('LEFT_DELIMITER');
	//结束定界符
	$smarty->right_delimiter = C('RIGHT_DELIMITER');
	//是否缓存
	$smarty->caching = C('CACHE_SWITCH');
	//缓存目录
	$smarty->cache_dir = APP_CACHE_PATH;
	//缓存时间
	$smarty->cache_lifetime = C('CACHE_TIME');
	self::$smarty = $smarty;

}
//分配变量***************************************************
protected function assign($var,$value){
	self::$smarty->assign($var,$value);
}
//载入模板 **************************************************
	/**
	 * [display 载入模板]
	 * @return [type] [description]
	 */
	protected function display($tpl=NULL){
		//误区：之前我们一直都是直接载入目标模板文件，但是，现在，我们是在缓存文件中读取，所以，我们最后还要开启缓存
		//得到返出的模板路径
		$path = $this->_get_tpl($tpl);
		//载入模板，填入$_SERVER['REQUEST_URI']是为了防止编绎后的文件不会出现重名覆盖的情况
		self::$smarty->display($path,$_SERVER['REQUEST_URI']);

	}

//缓存处理《主要是对缓存是否失效进行个判断》***********************************************
	protected function is_cached($tpl=NULL){
		//如果配置项中的CACHE中的文件设置为FALSE，也就是假值，那就会提示需要开启缓存
		if(!C('CACHE_SWITCH')) halt('请开启SMARTY缓存');
		//获得$path路径
		$path = $this->_get_tpl($tpl);
		//判断缓存是否失效
		return self::$smarty->is_cached($path,$_SERVER);
	}

//获得模板路径 **************************************************************
private function _get_tpl($tpl){
	//如果传入的模板文件为空
	if(is_null($tpl)){
		//组合模板路径
		$path = APP_TPL_PATH . '/' .CONTROL. '/' .METHOD . '.html';
	}else{
		// 如查传入模板文件，就用用户传入的用户文件
		// 模板有.html和.htm两种，取出后缀
		$suffix  = ltrim(strrchr($tpl,'.'),'.');
		//如果直接输入的是INDEX，那SUFFIX就为假，那就给它加个.html<IF后面为真，才会执行>
		if(!$suffix) $tpl .= '.html';
		//组合模板路径
		$path = APP_TPL_PATH . '/' .CONTROL.'/' . $tpl;
	}
	return $path;
}






}

 ?>