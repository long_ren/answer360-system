<?php
/* 
* [***********************]
* @author: mazhenyu[houdunwangmzy@163.com]
* @Date:   2014-05-22 17:03:01
* @Last Modified time: 2014-05-26 13:46:23
*/
class Application{
	/**
	 * [run 应用提供给外部执行方法]
	 * @return [type] [description]
	 */
	public static function run(){
		//初始化框架
		self::_init();
		//设置外部路径
		self::_set_url();
		//自动载入
		spl_autoload_register(array(__CLASS__,'_autoload'));
		//创建DEMO
		self::_create_demo();
		//执行实例化
		self::_app_run();
	}

	/**
	 * [_init 初始化框架]
	 * @return [type] [description]
	 */
	private static function _init(){
		//框架配置项
		$sysConfig = include C34_PATH . '/Config/config.php';
		//用户配置项
		$userPath = APP_CONFIG_PATH . '/config.php';
		is_file($userPath) || file_put_contents($userPath, "<?php\r\nreturn array();");
		$userConfig = include $userPath;

		//加载配置项
		C($sysConfig);
		C($userConfig);

		//设置默认时区
		date_default_timezone_set(C('TIMEZONE'));
		//开启session
		session_start();
	}
	/**
	 * [_set_url 设置外部路径]
	 */
	private static function _set_url(){
		// p($_SERVER);
		define('__APP__', 'http://'. $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME']);
		//定义tpl外部路径，为了使用FRAME框架文件更加方便
		define('__TPL__', dirname(__APP__) . '/' . APP_NAME . '/Tpl');
		//定义PUBLIC外部路径，为了使用CSS,JS,IMAGES
		define('__PUBLIC__', dirname(__APP__) . '/' . APP_NAME . '/Tpl/Public');

	
	}
	/**
	 * [_autoload 自动载入]
	 * @param  [type] $className [description]
	 * @return [type]            [description]
	 */
	private static function _autoload($className){
		//检测是否为控制器
		if(strlen($className) > 7 && substr($className, -7) == 'Control'){
			$path = APP_CONTROL_PATH . '/' . $className . ".class.php";
		}else{
			$path = TOOL_PATH . '/' . $className . '.class.php';
		}
		//载入
		include $path;
	}

	/**
	 * [_create_demo 创建演示DEMO]
	 * @return [type] [description]
	 */
	private static function _create_demo(){
		$path = APP_CONTROL_PATH . '/IndexControl.class.php';
		$data = <<<str
<?php
class IndexControl extends Control{
	public function index(){
		header('Content-type:text/html;charset=utf-8');
		echo '<h2 style="margin:50px">欢迎使用后盾34班，超级框架！(: !</h2>';
	}
}
str;
		is_file($path) || file_put_contents($path, $data);
	}

	private static function _app_run(){
		//控制器
		$control = isset($_GET['c']) ? htmlspecialchars($_GET['c']) : 'Index';
		//方法
		$method = isset($_GET['m']) ? htmlspecialchars($_GET['m']) : 'index';

		//定义控制器常量
		define('CONTROL', $control);
		//定义方法常量
		define('METHOD', $method);

		//连接control
		$control = $control . 'Control';

		//实例化对象，执行方法
		$obj = new $control;
		//判断对象里面的方法是否存在
		if(!method_exists($obj, $method)){
			halt($control . '控制器里面的' . $method . '方法不存在！:(');
		}else{
			$obj->$method();
		}



	
	}
}












