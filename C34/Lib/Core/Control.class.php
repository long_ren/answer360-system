<?php
/* 
* [***********************]
* @author: mazhenyu[houdunwangmzy@163.com]
* @Date:   2014-05-22 17:37:02
* @Last Modified time: 2014-05-26 13:52:50
*/
class Control extends SmartyView{
	private $var = array();
	/**
	 * [success 成功方法]
	 * @param  [type] $msg [description]
	 * @param  [type] $url [description]
	 * @return [type]      [description]
	 */
	protected function success($msg,$url){

		echo "<script>alert('$msg');location.href='$url'</script>";
		die;
	}
	/**
	 * [error 错误方法]
	 * @param  [type] $msg [description]
	 * @return [type]      [description]
	 */
	protected function error($msg){
		echo "<script>alert('$msg');history.back();</script>";
		die;
	}


}












