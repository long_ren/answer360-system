<?php
/* 
* [函数库]
* @author: mazhenyu[houdunwangmzy@163.com]
* @Date:   2014-05-08 14:38:07
* @Last Modified time: 2014-05-26 16:16:36
*/
/**
 * [p 打印函数]
 * @param  [type] $arr [description]
 * @return [type]      [description]
 */
function p($arr){
	echo '<pre>';
	print_r($arr);
	echo '</pre>';
}
/**
 * [d 打印函数]
 * @param  [type] $arr [description]
 * @return [type]      [description]
 */
function d($arr){
	var_dump($arr);
}
/**
 * [get_time 获得脚本时间]
 * @param  integer $switch [description]
 * @return [type]          [description]
 */
function get_time($switch = 0){
	static $start;
	if($switch == 1){
		//开始，保存第一次时间
		$start = microtime(true);
	}else{
		//所算的的时间差
		return  (microtime(true) - $start) * 1000 . 'ms';
		
	}
}

/**
 * [arr_low_up 把数组所有键名转为大写或者小写]
 * @param  [array] $arr  [所要转换的数组]
 * @param  [const] $case [大写或者小写]
 * @return [array]       [返回转换好的数组]
 */
function arr_low_up($arr,$case = CASE_UPPER){
	$arr = array_change_key_case($arr,$case);
	foreach ($arr as $k => $v) {
		if(is_array($v)){
			$arr[$k] = arr_low_up($v,$case);
		}
	}
	return $arr;
}

/**
 * [C 读取配置项函数]
 * @param [type] $var   [description]
 * @param [type] $value [description]
 */
function C($var = NULL,$value = NULL){
	//声明静态变量
	static $config = array();
	if(is_array($var)){
		$config = array_merge($config,array_change_key_case($var,CASE_UPPER));
		return;
	}

	if(is_string($var)){
		$var = strtoupper($var);
	
		if(!is_null($value)){
			$config[$var] = $value;
			return;
		}
		return isset($config[$var]) ? $config[$var] : NULL;
	}

	if(is_null($value)){
		return $config;
	}

}


function format_size($total){
	switch (true) {
		case $total > pow(1024, 3);
			$unit = array(3, 'G');
			break;

		case $total > pow(1024, 2);
			$unit = array(2, 'M');
			break;

		case $total > pow(1024, 1);
			$unit = array(1, 'KB');
			break;
		
		default:
			$unit = array(0, 'B');
			break;
	}
	return round($total / pow(1024, $unit[0]),1) . $unit[1];
}

/**
 * [del 删除文件夹]
 * @param  [type] $dirName [description]
 * @return [type]          [description]
 */
function del($dirName) { 
	if (!is_dir($dirName)) return false;

	foreach (glob($dirName. "/*") as $v){
		is_dir($v) ? del($v) : unlink($v);
	} 
	return rmdir($dirName);
}

/*function cp($source,$dest){ 
	if(!is_dir($source)) return false;
	//统一路径
	//源路径
	$source = change_path($source);
	//目标路径
	$dest = change_path($dest);

	foreach (glob($source . '*') as $v) {
		//组合新路径
		$newDest = $dest . basename($v);
		is_dir($v) && mkdir($newDest,0777,true);
		is_dir($v) ? cp($v,$newDest) : copy($v, $newDest);
	}

}*/
function cp($source,$dest){ 
	if(!is_dir($source)) return false;
	//统一路径
	//源路径
	$source = change_path($source);
	//目标路径
	$dest = change_path($dest);
	//组合新路径
	$newDest = $dest . basename($source);
	is_dir($newDest) || mkdir($newDest,0777,true);

	foreach (glob($source . '*') as $v) {
		is_dir($v) ? cp($v,$newDest) : copy($v, $newDest . basename($v));
	}

}

/**
 * [change_path 替换路径,规范路径，保证windows与linux系统都可以正常使用]
 * @param  [type] $path [description]
 * @return [type]       [description]
 */
function change_path($path){
	$path = str_replace('\\', '/', $path);
	$path = rtrim($path, '/') . '/';
	return $path;
}

function move($source,$dest){
	cp($source,$dest);
	del($source);

}



/**
 * [get_size description]
 * @return [type] [description]
 */
function get_size($path){
	//统一路径
	$path = str_replace('\\', '/', $path);
	$path = rtrim($path,'/') . '/';
	$size = 0;
	foreach (glob($path . '*') as $v) {
		$size += is_dir($v) ? get_size($v) : filesize($v);
	}
	return $size;
}
// function type_ok($name){
// 	$type = ;
	
// 	return '可以上传';
// }


function success($msg,$url){

	echo "<script>alert('$msg');location.href='$url'</script>";
	die;
}

function error($msg){
	echo "<script>alert('$msg');history.back();</script>";
	die;
}
/**
 * [upload 上传]
 * @param  [type] $path [description]
 * @return [type]       [description]
 */
function upload($path){
	//重组数组
	$arr = reset_arr();
	//移动上传
	move_upload($arr,$path);
}

/**
 * [reset_arr 重组数组]
 * @return [type] [description]
 */
function reset_arr(){
	$tempArr = array();
	foreach ($_FILES as $v) {
		if(is_array($v['name'])){
			foreach ($v['name'] as $key => $value) {
				$tempArr[] = array(
					'name'		=>	$value,
					'type'		=>	$v['type'][$key],
					'tmp_name'	=>	$v['tmp_name'][$key],
					'error'		=>	$v['error'][$key],
					'size'		=>	$v['size'][$key]
					);
			}	
		}else{
			$tempArr[] = $v;
		}
		
	}
	return $tempArr;
}

/**
 * [move_upload 上传移动]
 * @return [type] [description]
 */
function move_upload($arr,$path){
	foreach ($arr as $k => $v) {
		if(is_uploaded_file($v['tmp_name'])){
			//获得文件类型
			$fileInfo = pathinfo($v['name']);
			$type = $fileInfo['extension'];
			//组合文件名
			$fileName = time() . mt_rand(0,99999) . '.' . $type;
			//规范路径
			$path = change_path($path);
			//建立文件夹
			is_dir($path) || mkdir($path,0777,true);
			//完整路径
			$fullPath = $path . $fileName;
			//移动上传文件
			move_uploaded_file($v['tmp_name'], $fullPath);
		}
	}
}

/**
 * [halt 终止方法]
 * @param  [type] $msg [description]
 * @return [type]      [description]
 */
function halt($msg){
	header('Content-type:text/html;charset=utf-8');
	echo $msg;die;
}

/**
 * [get_const 打印自己框架所定义的常量]
 * @return [type] [description]
 */
function get_const(){
	$const = get_defined_constants(true);
	p($const['user']);
}

/**
 * [M函数]
 * @return [type] [description]
 */
function M($table){
		//执行MODEL类
		$model = new Model($table);	
	// 返出对像,将NWE后的MODEL对象付给$MODEL.换句话说，就是M()函数就是为了在外面不用再次的NEW MODEL类，让我们更方便的对数据进行处理
	return $model;
}
// 网页跳转
function go($url){
	header('location:'.$url);
	die;
}
















