<?php
/* 
* [***********************]
* @author: mazhenyu[houdunwangmzy@163.com]
* @Date:   2014-05-22 15:55:53
* @Last Modified time: 2014-05-23 14:03:27
*/
return array(
	'TIMEZONE'	=>	'PRC',
	'CODE_LEN'		=>	4,
	'CODE_FONTSIZE'	=>	20,
	'CODE_BGCOLOR'	=>	'#ffffff',
	'CODE_COLOR'	=>	'#000000',
	'CODE_FILE'		=>	FONT_PATH . '/font.ttf',
	'CODE_SEED'		=>	'1234567890qwtyuiopasdfghjklzxcvbnm',
	'CODE_WIDTH'	=>	160,
	'CODE_HEIGHT'	=>	40,
	// ***************************************** 
	// 数据库配置项
	'DB_HOST'		=>	'localhost',
	'DB_USER'		=>	'root',
	'DB_PASSWORD'	=>	'',
	'DB_NAME'		=>	'',
	'DB_CHARSET'	=>	'UTF8',
	'DB_PREFIX'		=>	'',
	//SMARTY ***************************************** 
	'LEFT_DELIMITER'	=>'{',
	'RIGHT_DELIMITER'	=>'}',
	'CACHE_SWITCH'		=>false,
	'CACHE_TIME'		=>'10',
	);
?>
