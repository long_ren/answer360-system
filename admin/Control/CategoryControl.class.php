<?php
// 问题分类*************************************
class CategoryControl extends CommonControl{
	public function index(){
		//显示所有的顶级分类
		//SQL查询语句
		$sql = "SELECT * FROM hd_category";
		//执行SQL语句
		$data = M('wenda')->query($sql);
		$data=$this->limitless($data);
		$category_top=$this->unlimitedForLayer($data,'Layer');
		//分配变量
		// var_dump($category_top);
		$this->assign('category_top',$category_top);
		$this->display('category.html');
	}
	// 增加子级分类*************************************
	public function add_cate_small(){
		if(IS_POST){
			//获得当前POST 的数据
		$title = htmlspecialchars($_POST['title']);
		//PID为顶级分类的
		$pid = htmlspecialchars($_POST['cid']);
		//拼合SQL
		$sql = "INSERT INTO hd_category SET title='{$title}',pid='{$pid}'";
		// var_dump($sql);die();
		//插入数据
			$uid =M('wenda')->exe($sql);
			//判断
			if($uid>0){
				$this->success('分类添加成功','admin.php?c=Category');

			}else{
				$this->error('分类添加失败');
			}

		}
		
		$this->display('add_cate.html');
	}
	//显示子级分类
	public function index_small_list(){
		$pid = $_GET['k'];
		$topcate = $_GET['cate'];
		//获得数据库的数据
		//拼合MYSQL语句
		$sql = "SELECT *FROM hd_category WHERE pid={$pid}";
		//查询数据
		$category_small = M('wenda')->query($sql);
		//分配变量
		$this->assign('category_small',$category_small);
		$this->assign('topcate',$topcate);
		$this->display('category_small.html');
	}
	//删除分类
	public function delete_list(){
		//获得分类的PID
		$pid = $_GET['k'];
		//获得分类的名称
		$title = $_GET['title'];
		//获得数据库的数据
		//拼合MYSQL语句
		$sql = "DELETE FROM hd_category WHERE pid={$pid} AND title='{$title}'";
		//数据删除
		$uid =M('wenda')->exe($sql);
			//判断
			if($uid>0){
				$this->success('删除成功','admin.php?c=Category');
			}else{
				$this->error('删除失败');
			}
		
	}
	//修改分类 ***************************************
	public function eait_list(){
		if(IS_POST){
		//获得分类的PID
		$cid = $_POST['cid'];
		//获得用用户传进来POST数据
		$title = $_POST['title'];
		//拼合MYSQL语句
		$sql = "UPDATE hd_category SET title='{$title}' WHERE cid={$cid}";
		//数据删除
		$uid =M('wenda')->exe($sql);
			//判断
			if($uid>0){
				$this->success('修改成功','admin.php?c=Category');
			}else{
				$this->error('修改失败');
			}

		}
		
	$this->display('edit_cate.html');
	}
//添加顶级分类
	public function add_cate_top(){
		if(IS_POST){
			//获得当前POST 的数据
			$title = $_POST['title'];
			//PID为顶级分类的
			$pid = 0;
			//拼合SQL
			$sql = "INSERT INTO hd_category SET title='{$title}',pid='{$pid}'";
			//判断
			$uid =M('wenda')->exe($sql);
			if($uid>0){
				$this->success('分类添加成功','admin.php?c=Category&m=add_cate_top');

			}else{
				$this->error('分类添加失败');
			}
			
		}
		$this->display('add_top_cate');
	}

}