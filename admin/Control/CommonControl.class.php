<?php
class CommonControl extends Control{
	// 构造韩数，判断是否登陆
	public function __construct(){
		parent::__construct();
		// 	//判断SESSION是否存在，如果存在，表示已登陆！
		if(!isset($_SESSION['adminName'])) go(__APP__ . '?c=Login');
	}
	/**
	 * 递归重新排序无限极分类数组
	 */
	//传入分类数据和PID，默认PID为0《PID=顶级分类》
	// public function limitless($data,$pid=0){
	// 	// 定义空数组
	// 	$arr = array();
	// 	foreach ($data as $v) {
	// 		if($v['pid']==$pid){
	// 			// 如果PID相同，压入新数组
	// 			$arr[]=$v;
	// 			$arr = array_merge($arr,$this->limitless($data,$v['cid']));
	// 		}
	// 	}
	// 	return $arr;
	// }
	 public function limitless($data,$html='---',$pid=0,$level=0){
    	//定义一个空数组
    	$arr = array();
    	//遍历数组
    	foreach ($data as $v) {
    		//如果遍历出的pid键值与参数pid相等
    		if ($v['pid'] == $pid) {
                //将参数等级压入数组
                $v['level'] = $level + 1;
                $v['html'] = str_repeat($html, $level);
    			//将得到的结果压入数组末尾
    			$arr[] = $v;
    			//合并数组
    			$arr = array_merge($arr,$this->limitless($data,$html,$v['cid'],$level + 1));
    		}
    	}
    	return $arr;
    }

    public function unlimitedForLayer($data,$name,$pid=0){
        //定义一个空数组
        $arr = array();
        //遍历数组
        foreach ($data as $v) {
            //如果与参数pid相等
            if ($v['pid'] == $pid) {
                //将递归调用的结果赋值给数组
                $v[$name] = $this->unlimitedForLayer($data,$name,$v['cid']);
                //将组合好的数组压入空数组
                $arr[] = $v;
            }
        }
        return $arr;
    }

	
}
?>