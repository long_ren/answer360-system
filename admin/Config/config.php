<?php
/* 
* [***********************]
* @author: mazhenyu[houdunwangmzy@163.com]
* @Date:   2014-05-22 15:55:53
* @Last Modified time: 2014-05-23 14:03:27
*/
$config =  array(
	'TIMEZONE'	=>	'PRC',
	'CODE_LEN'		=>	1,
	'CODE_FONTSIZE'	=>	15,
	'CODE_BGCOLOR'	=>	'#ffffff',
	'CODE_COLOR'	=>	'#000000',
	'CODE_FILE'		=>	FONT_PATH . '/font.ttf',
	'CODE_SEED'		=>	'1234567890qwtyuiopasdfghjklzxcvbnm',
	'CODE_WIDTH'	=>	100,
	'CODE_HEIGHT'	=>	40,
	// ***************************************** 
	// 数据库配置项
	'DB_HOST'		=>	'localhost',
	'DB_USER'		=>	'root',
	'DB_PASSWORD'	=>	'',
	'DB_NAME'		=>	'wenda',
	'DB_CHARSET'	=>	'UTF8',
	'DB_PREFIX'		=>	'',
	//SMARTY ***************************************** 
	'LEFT_DELIMITER'	=>'{',
	'RIGHT_DELIMITER'	=>'}',
	'CACHE_SWITCH'		=>false,
	'CACHE_TIME'		=>'10',
	//*******************文件上传*******************
	'UPLOAD_TYPE'	=>	array('gif','png','jpg','bmp'),
	'UPLOAD_SIZE'	=>	3000000,
	'UPLOAD_FILE'	=>	ROOT_PATH . '/upload/' . date('ymd')
	);
 // 合并前面的数据，让前后台配置项，共用一个数据库
 return array_merge($config,include ROOT_PATH . '/Config/dbconfig.php');
?>
?>
