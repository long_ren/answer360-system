<?php /* Smarty version 2.6.26, created on 2014-06-09 21:55:51
         compiled from C:/wamp/www/360/admin/Tpl/Member/user_list.html */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
	<title></title>
	<link rel="stylesheet" href="<?php echo @__PUBLIC__; ?>
/Css/public.css" />
	<script type="text/javascript" src="./Js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="./Js/public.js"></script>	
</head>

<body>
	<table class="table">
		<tr>
			<td class="th" colspan="20">用户列表</td>
		</tr>
		<?php $_from = $this->_tpl_vars['member']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
		<tr class="tableTop">
			<td class="tdLittle1">ID</td>
			<td>用户名</td>
			<td>状态管理</td>
			<td>回答数</td>
			<td>被采纳数</td>
			<td>提问数</td>
			<td>金币</td>
			<td>经验</td>
			<td>最后登录时间</td>
			<td>最后登录IP</td>
			<td>注册时间</td>
			<td>帐号状态</td>
			<td>操作</td>
		</tr>

		<tr>
		
			<td><?php echo $this->_tpl_vars['v']['uid']; ?>
</td>
			<td><?php echo $this->_tpl_vars['v']['username']; ?>
</td>
			<?php if ($this->_tpl_vars['v']['power'] != 0): ?>
			<td><a href="admin.php?c=Member&m=point_message&username=<?php echo $this->_tpl_vars['v']['username']; ?>
" >指定为管理员</a>
			</td>
			<?php else: ?>
			<td>
			<a href="admin.php?c=Member&m=point_member&username=<?php echo $this->_tpl_vars['v']['username']; ?>
" style="color:#50C167;">降低为会员</a>
			<?php endif; ?>
			
			
			</td>
			<td><?php echo $this->_tpl_vars['v']['answer']; ?>
</td>
			<td><?php echo $this->_tpl_vars['v']['accept']; ?>
</td>
			<td><?php echo $this->_tpl_vars['v']['ask']; ?>
</td>
			<td><?php echo $this->_tpl_vars['v']['point']; ?>
</td>
			<td><?php echo $this->_tpl_vars['v']['exp']; ?>
</td>
			<td><?php echo $this->_tpl_vars['v']['logintime']; ?>
</td>
			<td><?php echo $this->_tpl_vars['v']['loginip']; ?>
</td>
			
			<td></td>
			<?php if ($this->_tpl_vars['v']['power'] == 0): ?>
			<td style='color:red;'>后台管理员</td>
			<?php else: ?>
			<td style="color:#50C167;">会员用户</td>
			<?php endif; ?>
			
		</tr>
		<?php endforeach; endif; unset($_from); ?>
	</table>
		
</body>
</html>