$(function(){
	//定义一个id
	cateID = 0;
	//select元素的值发生改变时调用函数
	$('select[name=cate-one]').change(function(){
		//取出当前对象
		var obj = $(this);
		var cid = obj.val();
		//发送异步
		$.ajax({
			//请求地址
			url : APP + '?c=Ask&m=get_cate',
			//发送类型
			type : 'post',
			//发送数据
			data : {'cid' : cid},
			//服务器返回数据类型
			dataType : 'json',
			//返回数据
			success:function(data){
				//如果data为真
				if(data){
					//定义空选项
					var option = '';
					//对data的所有子元素进行方法调用
					$.each(data,function(i,k){
						//组合option选项
						option += '<option value="' + k.cid + '">' + k.title + '</option>';
					})
					//显示option选项
					obj.next().html(option).show();
				}
			}
		})
		//给id赋值
		cateID = obj.val();
	})

	//点击确定触发的事件
	$('#ok').click(function(){
		//如果id没有值
		if(!cateID){
			alert('请选择一个分类');
			return;
		}
		//给隐藏input加cid
		$('input[name = cid]').val(cateID);
		$('.close-window').click();
	})

	//判断内容是否完整
	$('.send-btn').click(function(){

		var cons = $('textarea[name=content]');
		//如果内容为空
		if(cons.val() == ''){
			alert('请输入提问内容！');
			cons.focus();
			return false;
		}

		//如果没有ID说明没有选择分类
		if(!cateID) {
			alert('请选择一个分类');
			return false;
		}
	})
})