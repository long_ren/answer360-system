<?php
/* 
* [***********************]
* @author: mazhenyu[houdunwangmzy@163.com]
* @Date:   2014-05-22 15:55:53
* @Last Modified time: 2014-05-23 14:03:27
*/
 $config = array(
	//*****************时区*****************
		'TIMEZONE'	=>	'PRC',
		//********************验证码**************
		'CODE_LEN'		=>	2,
		'CODE_FONTSIZE'	=>	14,
		'CODE_BGCOLOR'	=>	'#e7e7e7',
		'CODE_COLOR'	=>	'#ddd',
		'CODE_FILE'		=>	FONT_PATH . '/font.ttf',
		'CODE_SEED'		=>	'1234567890qwtyuiopasdfghjklzxcvbnm',
		'CODE_WIDTH'	=>	100,
		'CODE_HEIGHT'	=>	30,
		//*****************数据库信息*****************
		'DB_HOST'		=> '127.0.0.1',
		'DB_USER'		=> 'root',
		'DB_PASWD'		=> '',
		'DB_NAME'		=> 'wenda',
		'DB_CHARSET'	=> 'UTF8',
		'DB_PREFIX'		=> '',
		//*******************smarty*******************
		'LEFT_DELIMITER'	=> '{',
		'RIGHT_DELIMITER'	=> '}',
		'CACHE_SWITCH'		=>	false,
		'CACHE_TIME'		=>	10,
		//*******************文件上传*******************
		'UPLOAD_TYPE'	=>	array('gif','png','jpg','bmp'),
		'UPLOAD_SIZE'	=>	3000000,
		'UPLOAD_FILE'	=>	ROOT_PATH . '/upload/' . date('ymd')
	);
 // 合并前面的数据，让前后台配置项，共用一个数据库
 return array_merge($config,include ROOT_PATH . '/Config/dbconfig.php');
?>
