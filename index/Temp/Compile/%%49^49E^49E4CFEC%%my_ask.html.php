<?php /* Smarty version 2.6.26, created on 2014-06-10 17:46:02
         compiled from C:/wamp/www/360/index/Tpl/Member/my_ask.html */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "../Index/header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>后盾问答</title>
	<meta name="keywords" content="后盾问答"/>
	<meta name="description" content="后盾问答"/>
	<link rel="stylesheet" href="./Css/common.css" />
	<script type="text/javascript" src='./Js/jquery-1.7.2.min.js'></script>
	<script type="text/javascript" src='./Js/top-bar.js'></script>
	<link rel="stylesheet" href="<?php echo @__PUBLIC__; ?>
/css/member.css" />
	<script type="text/javascript" src='./Js/member.js'></script>
</head>
<body>
<div class="SSwd">	
<!--------------------中部-------------------->
<div id='center'>
<!-- 左侧 -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "./left_menu.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<!-- 左侧结束 -->
		<div id='right'>
			<p class='title'>我的提问</p>
			<ul class='ask-filter'>
				<li class='cur'>待解决问题</li>
				<li>已解决问题</li>
			</ul>
			<div class='list list-filter'>
				<table>
					<tr height='140'>
						<td>你还没有待解决的提问</td>
					</tr>
					<tr>
						<th class='t1'>标题</th>
						<th>回答数</th>
						<th class='t3'>更新时间</th>
					</tr>
					<tr>
						<td class='t1'>
							<a href="">什么牌子的电脑好</a><span>[电脑/硬件]</span>
						</td>
						<td>20</td>
						<td class='t3'>2014-9-4</td>
					</tr>
				</table>
			</div>
			<div class='list list-filter hidden'>
				<table>
					<tr height='140'>
						<td>你还没有已解决的提问</td>
					</tr>
					<tr>
						<th class='t1'>标题</th>
						<th>回答</th>
						<th class='t3'>更新时间</th>
					</tr>

					<tr>
						<td class='t1'>
							<a href=""></a><span>[电脑/硬件]</span>
						</td>
						<td>20</td>
						<td class='t3'>2014-9-4</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>
<!--------------------中部结束-------------------->
<!-- 底部 -->
	<div id='bottom'>
		<p>Copyright © 2013 Qihoo.Com All Rights Reserved 后盾网</p>
		<p>京公安网备xxxxxxxxxxxx</p>
	</div>
<!--[if IE 6]>
    <script type="text/javascript" src="./Js/iepng.js"></script>
    <script type="text/javascript">
    	DD_belatedPNG.fix('.logo','background');
        DD_belatedPNG.fix('.nav-sel a','background');
        DD_belatedPNG.fix('.ask-cate i','background');
    </script>
<![endif]-->
</body>
</html>
<!-- 底部结束 -->