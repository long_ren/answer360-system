<?php /* Smarty version 2.6.26, created on 2014-07-31 10:52:27
         compiled from ./header_big.html */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Document</title>
<link rel="stylesheet" href="<?php echo @__PUBLIC__; ?>
/css/common.css" />
<link rel="stylesheet" href="<?php echo @__PUBLIC__; ?>
/css/common_self.css" />
<link rel="stylesheet" href="<?php echo @__PUBLIC__; ?>
/css/ask.css" />
<link rel="stylesheet" href="<?php echo @__PUBLIC__; ?>
/css/index.css" />
<link rel="stylesheet" href="./Css/member.css" />
<script type="text/javascript" src="<?php echo @__PUBLIC__; ?>
/js/jquery-1.7.2.min.js"></script>
 <script type="text/javascript" src="<?php echo @__PUBLIC__; ?>
/js/model.js"></script>
<script type="text/javascript" src='<?php echo @__PUBLIC__; ?>
/js/index.js'></script>
<script type="text/javascript" src='<?php echo @__PUBLIC__; ?>
/js/top-bar.js'></script>
<script type="text/javascript">
	// 定义一个变量，方便JS片处理<js中不认识APP常量>
		var APP = '<?php echo @__APP__; ?>
';
	</script>
<script type="text/javascript" src='<?php echo @__PUBLIC__; ?>
/js/ask.js'></script>


</head>
<body>
<!-- 顶部，随拖拉条一起上下 -->
<div id="moveTop">
	<div class="Swd">
		<ul class="menu FL">  
			<li>
				<a href="index.php">首页	</a>
			</li> 
			<li>
				<a href="">新闻	</a>
			</li>
			<li>
				<a href="">问答	</a>
			</li>
			<li>
				<a href="">视频	</a>
			</li>
			<li>
				<a href="">图片	</a>
			</li>
			<li>
				<a href="">音乐	</a>
			</li>
			<li>
				<a href="">地图	</a>
			</li>
		</ul>
		<ul class='top-bar-right FR'>
		<?php if ($_SESSION['name'] != ''): ?>
		
				<li class='userinfo'>
					<!-- $icoinfo当前用户图片 -->
					<img src="<?php echo $this->_tpl_vars['icoinfo']; ?>
" alt="" />
					亲爱的会员：<a href="index.php?c=Member&m=index" class='uname' style='color:#37A508;padding-right:5px;line-height:35px;'><?php echo $_SESSION['name']; ?>
</a>
				</li>
				<li style='color:#eaeaf1'>|</li>
				<li>
					<a href="index.php?c=Index&m=logout" style='color:#ddd;'>安全退出</a>
				</li>
				<?php else: ?>

	<!-- 			<li class='userinfo'>
					<a href="" class='uname'></a>
				</li>
				<li style='color:#eaeaf1'>|</li>
				<li><a href="">退出</a></li> -->
		
				<li><a href="" class='login'>登录</a></li>
				<li style='color:#eaeaf1'>|</li>
				<li><a href="" class='register'>注册</a></li>	
				<?php endif; ?>
		</ul>
	</div>
</div>
<!----------登录框---------->	
	<div id='login' class='hidden'>
		<div class='login-title'>
			<p>欢迎您登录问答系统</p>
			<a href="" title='关闭' class='close-window'></a>
		</div>
		<div class='login-form'>
			<span id='login-msg'></span>
			<!-- 登录FORM -->
			<form action="index.php?c=Index&m=login" method='post' name='login'>
				<ul>
					<li>
						<label for="login-acc">账号</label>
						<input type="text" name='username' class='input' id='login-acc'/>
					</li>
					<li>
						<label for="login-pwd">密码</label>
						<input type="password" name='pwd' class='input' id='login-pwd'/>
					</li>
					<li class='login-auto'>
						<label for="auto-login">
							<input type="checkbox" checked='checked' name='auto'  id='auto-login'/>&nbsp;下一次自动登录
						</label>
						<a href="" id='regis-now'>注册新账号</a>
					</li>
					<li>
						<input type="submit" value='' id='login-btn'/>
					</li>
				</ul>
			</form>
		</div>
	</div>

<!--背景遮罩--><div id='background' class='hidden'></div>
<!----------注册框---------->
	<div id='register' class='hidden'>
		<div class='reg-title'>
			<p>欢迎注册后盾问答</p>
			<a href="" title='关闭' class='close-window'></a>
		</div>
		<div id='reg-wrap'>
			<div class='reg-left'>
				<ul>
					<li><span>账号注册</span></li>
				</ul>
				<div class='reg-l-bottom'>
					已有账号，<a href="" id='login-now'>马上登录</a>
				</div>
			</div>
			<div class='reg-right'>
				<form action="index.php?c=Index&m=reg" method='post' name='register'>
					<ul>
						<li>
							<label for="reg-uname">用户名</label>
							<input type="text" name='username' id='reg-uname'/>
							<span>2-14个字符：字母、数字或中文</span>
						</li>
						<li>
							<label for="reg-pwd">密码</label>
							<input type="password" name='pwd' id='reg-pwd'/>
							<span>6-20个字符:字母、数字或下划线 _</span>
						</li>
						<li>
							<label for="reg-pwded">确认密码</label>
							<input type="password" name='pwded' id='reg-pwded'/>
							<span>请再次输入密码</span>
						</li>
						<!-- <li>
							<label for="reg-verify">验证码</label>
							<input type="text" name='verify' id='reg-verify'/>
							<img onclick="this.src=this.src + '?' + Math.random()" src="<?php echo @__APP__; ?>
?c=Login&m=code&" width='99' height='35' alt="验证码" id='verify-img'/>
							<span>请输入图中的字母或数字，不区分大小写</span>
						</li> -->
						<li class='submit'>
							<input type="submit" value='立即注册'/>
						</li>
					</ul>
				</form>
			</div>
		</div>
	</div>
<!--------------------中部-------------------->
<div id="askquestion" class="hidden">
	<div id='center' class="cate" >
		<div class='send'>
			<div class='title'>
				<p class='left'>向亿万网友们提问</p>
				<p class='right'>您还可以输入<span id='num'>50</span>个字</p>
			</div>
			<form action="index.php?c=Ask&m=index" method='post'>
				<div class='cons'>
					<textarea name="content"></textarea>
				</div>
				<div class='reward'>
					<span id='sel-cate' class='cate-btn'>选择分类</span>
					<ul>
						<li>
							我的金币：<span>20</span>
						</li>
						<li>
					  悬赏：<select name="reward">
								<option value="0">0</option>
								<option value="5">5</option>
								<option value="10">10</option>
								<option value="15">15</option>
								<option value="20">20</option>
								<option value="30">30</option>
								<option value="50">50</option>
								<option value="80">80</option>
								<option value="100">100</option>
							</select>金币
						</li>
					</ul>
				</div>
				<input type='hidden' name='uid' value='<?php echo $_SESSION['uid']; ?>
'/>
				<input type="submit" value='提交问题' class='send-btn'/>
			
		</div>
	</div>
	<div id='category'>
		<p class='title'>
			<span>请选择分类</span>
			<a href="" class='close-window'></a>
		</p>
		<div class='sel'>
			<p>为您的问题选择一个合适的分类：</p>
			<select name="cate-one" size='16'>
			<?php $_from = $this->_tpl_vars['Left_menu']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
				<option value="<?php echo $this->_tpl_vars['v']['cid']; ?>
"><?php echo $this->_tpl_vars['v']['title']; ?>
</option>
			<?php endforeach; endif; unset($_from); ?>
			</select>
			<select name="cate-one" size='16' class='hidden'></select>
			<select name="cate-one" size='16' class='hidden'></select>
		</div>
		<p class='bottom'>
			<span id='ok'>确定</span>
		</p>
	</div>
</div>
</form>
<!--------------------中部结束-------------------->
<!-- logo和搜索区域 -->
<div class="Swd logoArea">
	<img src="<?php echo @__PUBLIC__; ?>
/images/logo.png" alt="" / class="logoIMG">
	<ul>
		<li>
			<form action="<?php echo @__APP__; ?>
?c=Search" method="post">
			<input type="text" / class="SearchInput" name="content" value="<?php echo $this->_tpl_vars['content']; ?>
">
		</li>
		<li class="Sub">
			<input type="submit" value="" / class="Sub">
			</form>
		</li>
		<li class="ask" id="ask">
			<a href=""></a>
		</li>
		
	</ul>
</div>
<div class="cleardiv"></div>
<!-- 导航条 -->
<div id="Nv" class="Awd">
	<div class="menu Swd">
		<ul>
			<li><a href="<?php echo @__APP__; ?>
">首页</a></li>
			<?php unset($this->_sections['n']);
$this->_sections['n']['loop'] = is_array($_loop=$this->_tpl_vars['top_banner']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['n']['name'] = 'n';
$this->_sections['n']['start'] = (int)2;
$this->_sections['n']['max'] = (int)5;
$this->_sections['n']['show'] = true;
if ($this->_sections['n']['max'] < 0)
    $this->_sections['n']['max'] = $this->_sections['n']['loop'];
$this->_sections['n']['step'] = 1;
if ($this->_sections['n']['start'] < 0)
    $this->_sections['n']['start'] = max($this->_sections['n']['step'] > 0 ? 0 : -1, $this->_sections['n']['loop'] + $this->_sections['n']['start']);
else
    $this->_sections['n']['start'] = min($this->_sections['n']['start'], $this->_sections['n']['step'] > 0 ? $this->_sections['n']['loop'] : $this->_sections['n']['loop']-1);
if ($this->_sections['n']['show']) {
    $this->_sections['n']['total'] = min(ceil(($this->_sections['n']['step'] > 0 ? $this->_sections['n']['loop'] - $this->_sections['n']['start'] : $this->_sections['n']['start']+1)/abs($this->_sections['n']['step'])), $this->_sections['n']['max']);
    if ($this->_sections['n']['total'] == 0)
        $this->_sections['n']['show'] = false;
} else
    $this->_sections['n']['total'] = 0;
if ($this->_sections['n']['show']):

            for ($this->_sections['n']['index'] = $this->_sections['n']['start'], $this->_sections['n']['iteration'] = 1;
                 $this->_sections['n']['iteration'] <= $this->_sections['n']['total'];
                 $this->_sections['n']['index'] += $this->_sections['n']['step'], $this->_sections['n']['iteration']++):
$this->_sections['n']['rownum'] = $this->_sections['n']['iteration'];
$this->_sections['n']['index_prev'] = $this->_sections['n']['index'] - $this->_sections['n']['step'];
$this->_sections['n']['index_next'] = $this->_sections['n']['index'] + $this->_sections['n']['step'];
$this->_sections['n']['first']      = ($this->_sections['n']['iteration'] == 1);
$this->_sections['n']['last']       = ($this->_sections['n']['iteration'] == $this->_sections['n']['total']);
?>
			<li class="question">
				<a href=""><?php echo $this->_tpl_vars['top_banner'][$this->_sections['n']['index']]['title']; ?>
</a>
				<!-- 二级导航，暂时隐藏 -->
				<div class="list" style="display:none">
				<?php unset($this->_sections['name']);
$this->_sections['name']['loop'] = is_array($_loop=$this->_tpl_vars['top_banner'][$this->_sections['n']['index']]['banner']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['name']['name'] = 'name';
$this->_sections['name']['max'] = (int)3;
$this->_sections['name']['show'] = true;
if ($this->_sections['name']['max'] < 0)
    $this->_sections['name']['max'] = $this->_sections['name']['loop'];
$this->_sections['name']['step'] = 1;
$this->_sections['name']['start'] = $this->_sections['name']['step'] > 0 ? 0 : $this->_sections['name']['loop']-1;
if ($this->_sections['name']['show']) {
    $this->_sections['name']['total'] = min(ceil(($this->_sections['name']['step'] > 0 ? $this->_sections['name']['loop'] - $this->_sections['name']['start'] : $this->_sections['name']['start']+1)/abs($this->_sections['name']['step'])), $this->_sections['name']['max']);
    if ($this->_sections['name']['total'] == 0)
        $this->_sections['name']['show'] = false;
} else
    $this->_sections['name']['total'] = 0;
if ($this->_sections['name']['show']):

            for ($this->_sections['name']['index'] = $this->_sections['name']['start'], $this->_sections['name']['iteration'] = 1;
                 $this->_sections['name']['iteration'] <= $this->_sections['name']['total'];
                 $this->_sections['name']['index'] += $this->_sections['name']['step'], $this->_sections['name']['iteration']++):
$this->_sections['name']['rownum'] = $this->_sections['name']['iteration'];
$this->_sections['name']['index_prev'] = $this->_sections['name']['index'] - $this->_sections['name']['step'];
$this->_sections['name']['index_next'] = $this->_sections['name']['index'] + $this->_sections['name']['step'];
$this->_sections['name']['first']      = ($this->_sections['name']['iteration'] == 1);
$this->_sections['name']['last']       = ($this->_sections['name']['iteration'] == $this->_sections['name']['total']);
?>
					<ul>
						<li><?php echo $this->_tpl_vars['top_banner'][$this->_sections['n']['index']]['banner']['name']['title']; ?>
</li>
					</ul>
				<?php endfor; endif; ?>
				</div>	
			</li>
			<?php endfor; endif; ?>
		<!-- 	<li><a href="">首页</a></li>
			<li><a href="">首页</a></li>
			<li><a href="">首页</a></li>
			<li><a href="">首页</a></li>
			<li><a href="">首页</a></li>
			<li><a href="">首页</a></li>
			<li><a href="">首页</a></li> -->

		</ul>
		<dl>
			<dd class="pc FL"><a href="">问答电脑版</a></dd>
			<dd class="phone FL"><a href="">问答手机版</a></dd>
			<dd class="help FL"><a href="">帮助</a></dd>
		</dl>
	</div>
</div>
	<div class="cleardiv"></div>
	