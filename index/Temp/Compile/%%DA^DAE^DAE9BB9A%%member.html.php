<?php /* Smarty version 2.6.26, created on 2014-06-11 12:36:06
         compiled from C:/wamp/www/360/index/Tpl/Member/member.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'C:/wamp/www/360/index/Tpl/Member/member.html', 47, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "../Index/header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>后盾问答</title>
	<meta name="keywords" content="后盾问答"/>
	<meta name="description" content="后盾问答"/>
	<link rel="stylesheet" href="<?php echo @__PUBLIC__; ?>
/css/member.css" />
</head>
<body>
<div class="SSwd">	
<!--------------------中部-------------------->
<div id='center'>
<!-- 左侧 -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "./left_menu.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<!-- 左侧结束 -->
		<div id='right'>
	
			<p class='title'>我的首页</p>

			<ul class='property'>
				<li>金币：<span><?php echo $this->_tpl_vars['askinfo']['point']; ?>
</span></li>
				<li>经验值：<span><?php echo $this->_tpl_vars['askinfo']['exp']; ?>
</span></li>
				<li>采纳率：<span>100%</span></li>
			</ul>
			<div class='list'>
				<p><span>我的提问 <b>(共100条)</b></span><a href="">更多>></a></p>
				<table>
						<?php if ($this->_tpl_vars['askinfo']['ask'] == 0): ?>
						<tr height='140'>
							<td>你还没有进行过提问</td>
						</tr>
						<?php else: ?>
						
						<tr>
							<th class='t1'>标题</th>
							<th>回答数</th>
							<th class='t3'>更新时间</th>
						</tr>
					<?php $_from = $this->_tpl_vars['askquesinfo']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
						<tr>
							<td class='t1'>
								<a href=""><?php echo $this->_tpl_vars['v']['content']; ?>
</a><span>[电脑/硬件]</span>
							</td>
							<td><?php echo $this->_tpl_vars['v']['answer']; ?>
</td>
							<td class='t3'><?php echo ((is_array($_tmp=$this->_tpl_vars['v']['time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y-%m-%d") : smarty_modifier_date_format($_tmp, "%Y-%m-%d")); ?>
</td>
						</tr>
						<?php endforeach; endif; unset($_from); ?>
							<?php endif; ?>
				</table>
			</div>
			<div class='list'>
				<p><span>我的回答 <b>(共20条)</b></span><a href="">更多>></a></p>
				<table>

					<tr height='140'>
						<td>你还没有进行过回答</td>
					</tr>

					<tr>
						<th class='t1'>标题</th>
						<th>回答</th>
						<th class='t3'>更新时间</th>
					</tr>
<!-- 
					<tr>
						<td class='t1'>
							<a href="">什么牌子的电脑好</a><span>[电脑/硬件]</span>
						</td>
						<td>20</td>
						<td class='t3'>1900.1.1</td>
					</tr> -->

				</table>
			</div>
		</div>
	</div>
</div>
<!--------------------中部结束-------------------->

<!--------------------底部-------------------->
<!-- 底部 -->
	<div id='bottom'>
		<p>Copyright © 2013 Qihoo.Com All Rights Reserved 后盾网</p>
		<p>京公安网备xxxxxxxxxxxx</p>
	</div>
<!--[if IE 6]>
    <script type="text/javascript" src="./Js/iepng.js"></script>
    <script type="text/javascript">
    	DD_belatedPNG.fix('.logo','background');
        DD_belatedPNG.fix('.nav-sel a','background');
        DD_belatedPNG.fix('.ask-cate i','background');
    </script>
<![endif]-->
</body>
</html>
<!-- 底部结束 -->