<?php
class IndexControl extends CommonControl{

	public function index(){
		//导航栏数据
		$this->category_Menu();
		//回答最多问题的人********
			$this->MorePeople();
		//左侧的导航
			$sql = "SELECT * FROM hd_category WHERE pid=0";
			$Left_menu = M('stu') -> query($sql);
			//重组数组
			foreach ($Left_menu as $k => $v) {
				$Left_menu[$k]['down'] = M('stu') -> query("SELECT * FROM hd_category WHERE pid=" . $v['cid']);
			}
			//分配栏目变量
			$this -> assign('Left_menu',$Left_menu);
			//所有问题 ******************************************
			$sql = "SELECT *FROM hd_ask";
			$allask = M('wenda')->query($sql);
			$this->assign('allask',$allask);
			//问题是否已解决 ******************************************
			// 组合SQL语句
			$sql1 = "SELECT *FROM hd_ask WHERE solve=0 ORDER BY time desc";
			$ask = M('wenda')->query($sql1);
			$this->assign('ask',$ask);
			//高赏 *******************************************
			$sql2 = "SELECT *FROM hd_ask WHERE reward>0 ORDER BY reward desc";
			$money = M('wenda')->query($sql2);
			$this->assign('money',$money);
			//0回答 *******************************************
			$sql3 = "SELECT *FROM hd_ask WHERE answer=0 ORDER BY time desc";
			$zero_answer = M('wenda')->query($sql3);
			// var_dump($zero_answer);
			$this->assign('zero_answer',$zero_answer);

			// //当前用户信息
			if(isset($_SESSION['name'])){
				//用户信息处理
				$result = $this->CommonMemberinfo();
				// 用户图相
			
				// var_dump($picpath);
			//当前用户提问信息
			// $this->askinfo();
			}
			
			$this->display('index.html');
		}
// 登陆**********************************************
	public function login(){
		//获得当前POST 的数据
		$username = $_POST['username'];
		$passwd = md5($_POST['pwd']);
		//拼合SQL
		$sql = "SELECT passwd FROM hd_user WHERE username='{$username}'";
		//获得数据
		$data = M('wenda')->query($sql);
		if(empty($data)) $this->error('请检查是否开启键盘大小写');
		if($data[0]['passwd']==$passwd){
			//查出CID，以便后面使用并且查出的信息写入SEESION，以便后面的提问等相相操作
			//拼合SQL
			$uidsql = "SELECT * FROM hd_user WHERE username='{$username}'";
			//查出所能的数据
			$uid = M('wenda')->query($uidsql);
			//将数据库里面的三维变成二维，方便取出CID值
			$uid = current($uid);
			//写入SESSION
			$_SESSION['uid'] = $uid['uid'];
			$_SESSION['name']=$_POST['username'];

			$this->success('成功登陆','index.php');

		}else{
			$this->success('登陆出错,请检查是否开启键盘大小写','index.php');
		}

		
		
	}
// 注册**********************************************
	public function reg(){
		if(IS_POST){
			// $model = new Model();
			//获得数据，
			$username	= htmlspecialchars($_POST['username']);
			$passwd	= md5($_POST['pwd']); 
			//拼合SQL数据
			$sql = "INSERT INTO hd_user SET username='{$username}',passwd='{$passwd}'";
			//写入SQL
			$uid =M('wenda')->exe($sql);
			if($uid>0){
				$this->success('注册成功,','index.php');

			}else{
				error('注册失败');
			}
		
		}

	}

// 异步处理**********************************************
		public function ajax_data(){
			//得到异步过来的数据
			$username = htmlspecialchars($_POST['username']);
			// var_dump($_POST);

			//载入用户数据 
			$data = include APP_DATA_PATH  . '/db.php';
			//循环比对
			foreach ($data as $key => $value) {

				if($value['username'] == $username){
					echo 0;return;
				}
				
			}
		
			echo 1;
		}
// 退出**********************************************
		public function logout(){
			//删除SESSION数据文件
			session_destroy();
			//清空SEEION内存文件
			session_unset();
			$this->success('成功退出','index.php');
		}
		public function url(){
				//导航栏数据
			$this->category_Menu();
			//当前用户信息
			$this->CommonMemberinfo();
			//回答最多问题的人********
			$this->MorePeople();
			// 获得提交问题的数据
			$cid = htmlspecialchars($_GET['cid']);
			// var_dump($cid);
			// 拼合SQL
	// 通过CID查出类名********************************************************************
			$sql = "SELECT *FROM hd_category WHERE cid={$cid}";
			$url = M('wenda')->query($sql);
			//因为查出来的分类CID是唯一的，所以，可以把它用CURRENT处理成二维数组
			$url = current($url);
			//分配变量
			$this->assign('url',$url);
	// 通过父级的CID与PID匹配查出下面的子级分类名称****************************************************
			$sql = "SELECT *FROM hd_category WHERE pid={$cid}";
			// 查出子级分类
			$urlson = M('wenda')->query($sql);
			// 因为子类的数据有可能不是唯一的，所以不用CURRENT进行处理
			$this->assign('urlson',$urlson);
//相关类里面的问题处理****************************************
			//根据GET传过来的CID查找相关的问题
			// 

			$cid=htmlspecialchars($_GET['cid']);
			// 给合SQL
			$judge = htmlspecialchars($_GET['judge']);
			//get 参数大于号的处理，直接用GET无法获得大于号，SQL语句会报错
			if($judge== 'reward=10'){
				$judge = 'reward>5';
			}
			//未解决
			$cateasksql = "SELECT * FROM hd_ask WHERE cid={$cid} AND {$judge}";
			// var_dump($cateasksql);
			$cateask = M('wenda')->query($cateasksql);
			
			//分配变量
			$this->assign('cateask',$cateask);

			$this->display('list.html');

	
		}
		public function cateAsk(){


		}

		
}