<?php
class SearchControl extends CommonControl{
	public function index(){
		//导航栏数据
		$this->category_Menu();
		// //根据POST提交的数据，在数据里面查找
		if(IS_POST){
			$content = htmlspecialchars($_POST['content']);
			//组合SQL语句，因为是找答案，所以输入的是问题。先找到有没有此类的问题
			//用关联查询，我们要找出答案和问题
			// $sql = "SELECT * FROM hd_ask WHERE content like '%{$content}%'";
			$sql = "SELECT *,a.content as con FROM hd_ask as a join hd_answer as s on a.asid=s.asid WHERE a.content like '%{$content}%'";
			
			// 找出相关的问题
			$search = M('wenda')->query($sql);
			// var_dump($search);
			if(empty($search)){
				$this->assign('search',0);
			}else{
				$this->assign('search',$search);
			}
			//分配变量
			//把CONTENT分配出去，为的是，搜索失败时，出现控制内容的提示，并且POST表单有此文字
			$this->assign('content',$content);
			
			
		}
		
	$this->display('search.html');
		}
	
}