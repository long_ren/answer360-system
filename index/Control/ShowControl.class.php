<?php 
class ShowControl extends CommonControl{
	public function index(){
		//导航栏数据
		$this->category_Menu();
		//用户相关信息**************************************************
		//当前用户的相关信息
		$this->CommonMemberinfo();
		//回答最多问题的人********
		$this->MorePeople();

//问题发布者的相关信息***************************************************
		//得到GET传过来的问题ID
		$asid = htmlspecialchars($_GET['asid']);
		// 根据得到的GET参数，查出相应的问题发布人的相关信息<查出UID信息>
		$sql = "SELECT * FROM hd_ask WHERE asid = {$asid}";
		$data = M('stu') -> query($sql);
		// // 分配变量，把这些信息分配到发布人，发布时间，发布问题等相关信息显示出来
		// 取出UID信息,方便后面对返出的关联数组信息进行处理
		$data =current($data);
		$uid = $data['uid'];
		// 组合SQL语句,用UID关联用户表和问题表，查出相关信息
		$sql = "SELECT *FROM hd_user AS u JOIN hd_ask AS a ON u.uid=a.uid WHERE u.uid={$uid} AND a.asid={$asid}";
		$codata = M('wenda') -> query($sql);
	// var_dump($codata);
	// echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>";
		//返出的为三维数组，转成二维
		$codata = current($codata);
		// 将得到的用户信息和问题等相关信息分配出去
		$this->assign('memberinfo',$codata);
		
//问题的回答数量等相关信息*************************************
		// 根据前面的问题ID，得出问题ID所对应的回答
		$answesql = "SELECT * FROM hd_answer WHERE asid = {$asid}";
		$answerList = M('wenda') -> query($answesql);
	// var_dump($answerList);
	// 分配变量
		$this->assign('answerList',$answerList);

// 采纳处理************************************************
		// 获得当前问题的ID，然后查出所有的回答。再取出采购的答案
		$bestanswersql = "SELECT * FROM hd_answer WHERE asid = {$asid} AND accept=1";
		$bestanswer = M('wenda') -> query($bestanswersql);
	// var_dump($bestanswer);
		// 分配变量
		if(empty($bestanswer)){
			// 如果上面结果数组为空，那么分配的变量也为0。方便前端面调用
			$this->assign('bestanswer',0);
		}else{
			$this->assign('bestanswer',$bestanswer);
		}

		$this->display('Show.html');
	}
	//回答问题
	public function answer(){
// 回答问题**************************************************
	

		if(IS_POST){
	if(!isset($_SESSION['name'])) $this->error('请先登陆');
			$data =array(
				'content'		=>	htmlspecialchars($_POST['content']),
				'time'			=>	time(),
				'uid'			=>	htmlspecialchars($_SESSION['uid']),
				'asid'			=>	htmlspecialchars($_POST['asid']),
				'accept'		=>	0
				);
			
		// 分配UID方便前端面判断是不是自已来回答 ,如果是自已，也看不到回答框
			$this->assign('uid',$data['uid']);
		// 给合SQL 
		$sql = "INSERT INTO hd_answer (content,time,uid,asid,accept) VALUES ('{$data["content"]}',{$data["time"]},{$data["uid"]},{$data["asid"]},{$data["accept"]});";
		//将回答插入数据库
			$answer = M('weida') -> exe($sql);
			//判断是否插入成功
			if ($answer) {
				// **
				// **用户表	
				// **
				$uid = $_SESSION['uid'];
				//在HD_USER中要增加插入一条 回答数量
				// 先查出，回答数量
					$answersql = "SELECT*FROM hd_user WHERE uid={$uid}";
					// var_dump($answersql);
					// echo"..............";
					$answerno =M('wenda')->query($answersql); 
					$answernoall = current($answerno);
					// 每插入一条，就增加一条数量<用户表中>********************
					$answerno = $answernoall['answer']+1;
					// 组合插入数量的SQL
					$answersql = "UPDATE hd_user SET answer={$answerno} WHERE uid={$data['uid']}";
					M('stu') -> exe($answersql);
					//在HD_USER中要增加插入一条 经验数************************************* 
					// 每插入一条，就增加一条数量
					$exp = $answernoall['exp']+1;
					// 组合插入数量的SQL
					$expsql = "UPDATE hd_user SET exp={$exp} WHERE uid={$data['uid']}";
					M('stu') -> exe($expsql);
				// **
				// **问题表	
				// **
				// 每插入一条，就增加一条数量<问题表中>********************	
					// 根据ASID查出相关的问题表信息
					$askformsql = "SELECT*FROM hd_ask where asid={$data['asid']}";
					$askformsql =M('wenda')->query($askformsql);
					//转成二维数组，方便取值
					$askformsql = current($askformsql);
					//查出问题表中的回答数
					$askformanswer = $askformsql['answer']+1;
					// 得到的新的回答数，重新插入数据库
					$askformanswer = "UPDATE hd_ask SET answer={$askformanswer} WHERE asid={$data['asid']}";
					// var_dump($askformanswer);

				$sult = M('stu') -> exe($askformanswer);
				// var_dump($sult);
				// die();
					$this -> error('感谢您的执心回答！么么哒');
				}
				$this -> error('回答失败哦，么么哒');


			}	

				$this->display('Show.html');
		}

// 采纳处理************************************************
	public function accept(){
		// 获得当前的回答的ID
		// var_dump($_GET);
		$anid = htmlspecialchars($_GET['anid']);
		$accsql= "UPDATE hd_answer SET accept=1 WHERE anid={$anid}";
	       // var_dump($accsql);
		$result = M('wenda') -> exe($accsql);
		// var_dump($result);die();
		if($result){
			$this->error('采纳成功');
		}else{
			$this->error('您已采纳');
		}
	}		
	


}

 ?>