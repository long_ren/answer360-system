<?php 
class CommonControl extends Control{
	// 导航条数据获取**********************************************
		public function category_Menu(){
			//获得数据库的数据
			$sql = "SELECT *FROM hd_category";
			$top_banner = M('wenda')->query($sql);
			// 对数据进行处理《一级和二级分开》
			$top_banner = $this->menu($top_banner,'banner');
			//分配变量
			$this->assign('top_banner',$top_banner);

			}
 public function limitless($data,$html='---',$pid=0,$level=0){
    	//定义一个空数组
    	$arr = array();
    	//遍历数组
    	foreach ($data as $v) {
    		//如果遍历出的pid键值与参数pid相等
    		if ($v['pid'] == $pid) {
                //将参数等级压入数组
                $v['level'] = $level + 1;
                $v['html'] = str_repeat($html, $level);
    			//将得到的结果压入数组末尾
    			$arr[] = $v;
    			//合并数组
    			$arr = array_merge($arr,$this->limitless($data,$html,$v['cid'],$level + 1));
    		}
    	}
    	return $arr;
    }

    public function unlimitedForLayer($data,$name,$pid=0){
        //定义一个空数组
        $arr = array();
        //遍历数组
        foreach ($data as $v) {
            //如果与参数pid相等
            if ($v['pid'] == $pid) {
                //将递归调用的结果赋值给数组
                $v[$name] = $this->unlimitedForLayer($data,$name,$v['cid']);
                //将组合好的数组压入空数组
                $arr[] = $v;
            }
        }
        return $arr;
    }
    public function menu($data,$name='down'){
       	 foreach ($data as $k => $v) {
				$data[$k][$name] = M('stu') -> query("SELECT * FROM hd_category WHERE pid=" . $v['cid']);
			}
			return $data;
         
    }
    //当前用户信息
    public function CommonMemberinfo(){
//************************ 用户信息********************************************
        //根据SESSION里面的UID查询所以的相关信息
            //只有在登陆的情况下才会显示
     if(isset($_SESSION['uid'])){

            $uid = $_SESSION['uid'];
        // var_dump($_SESSION);
        $sql = "SELECT * FROM hd_user as u  WHERE u.uid={$uid}";
        // var_dump($sql);
        $now_memberCenter = M('wenda')->query($sql);
        
        // var_dump($askinfo);
        $now_memberCenter = current($now_memberCenter);
        // 分配变量
                //游客登陆时
         }else{
            $now_memberCenter = 0;
         }

         $this->assign('now_memberCenter',$now_memberCenter);
//************************ 用户图相********************************************
       //组合网站路径
         $webpath = dirname(__APP__).'/';
        // 把WEBPATH分配出去，方便SMARTY调用比对
        if(empty($now_memberCenter['face'])){
         //数据库中的FACE为空，就给张默认的图片
                $icoinfo = __PUBLIC__ . '/images/picico.png';
                 }else{
                      //数据库中的FACE不为空，就取数据库中的图片
                 $icoinfo = dirname(__APP__).'/'.$now_memberCenter['face'];
                 }
          $this->assign('icoinfo',$icoinfo);
//************************ 当前用户提问等相关信息********************************************
           //只有在登陆的情况下才会显示
     if(isset($_SESSION['uid']))
               {$uid = $_SESSION['uid'];
        //用UID把表进行关联//answer有重名，会覆盖，取个别名Uanswer
        $sql = "SELECT *,u.answer as Uanswer FROM  hd_user as u join hd_ask as k on u.uid=k.uid  where u.uid={$uid}";
        $askquesinfo = M('wenda')->query($sql);
                   // 分配变量
          $this->assign('askquesinfo',$askquesinfo);
                }
            //游客时的数据
     


        // 分配用户数据
         
        
    }
    //************************ 回答最多问题的人********************************************
    public function MorePeople(){
        //搜出，回答问题最多的人
        $moreanswer = "SELECT * FROM hd_user ORDER BY answer DESC LIMIT 1";
        $moreanswer = M('wenda')->query($moreanswer);
        $moreanswer = current($moreanswer);
        //分配变量
        $this->assign('moreanswer',$moreanswer);
//************************ 用户图相********************************************
       //组合网站路径
         $webpath = dirname(__APP__).'/';
        // 把WEBPATH分配出去，方便SMARTY调用比对
        if(empty($moreanswer['face'])){
         //数据库中的FACE为空，就给张默认的图片
                $icoinfo = __PUBLIC__ . '/images/picico.png';
                 }else{
                      //数据库中的FACE不为空，就取数据库中的图片
                 $icoinfo = dirname(__APP__).'/'.$moreanswer['face'];
                 }
          $this->assign('icoAnswerMore',$icoinfo);
//************************ 搜出，经验最多的人********************************************
        $moreexpsql = "SELECT * FROM hd_user ORDER BY exp DESC LIMIT 1";
        $moreexp = M('wenda')->query($moreexpsql);
        $moreexp = current($moreexp);
        // var_dump($moreexp);
        // 分配变量
        $this->assign('moreexp',$moreexp);
        //************************ 用户图相********************************************
       //组合网站路径
         $webpath = dirname(__APP__).'/';
        // 把WEBPATH分配出去，方便SMARTY调用比对
        if(empty($moreexp['face'])){
         //数据库中的FACE为空，就给张默认的图片
                $icoexp = __PUBLIC__ . '/images/picico.png';
                 }else{
                      //数据库中的FACE不为空，就取数据库中的图片
                 $icoexp = dirname(__APP__).'/'.$moreexp['face'];
                 }
          $this->assign('icoExpMore',$icoexp);
          
//************************ 助人光荣榜********************************************
            // 搜出回答过的人，取三条
        $moresql = "SELECT * FROM hd_user ORDER BY answer DESC LIMIT 4";
        $moreansweruser = M('wenda')->query($moresql);
        //分配变量
        $this->assign('moreansweruser',$moreansweruser);
        //************************ 用户图相********************************************
            //组合网站路径
         $webpath = dirname(__APP__).'/';
        // 把WEBPATH分配出去，方便SMARTY调用比对

         //数据库中的FACE为空，就给张默认的图片
         $icodefault = __PUBLIC__ . '/images/picico.png';
        //数据库中的FACE不为空，就取数据库中的图片
          $this->assign('icodefault',$icodefault);
          
       

//************************ 待解决的相关问题********************************************
        $waitAnswersql = "SELECT * FROM hd_ask WHERE answer=0";
        $waitYouAns = M('wenda')->query($waitAnswersql);
        //分配变量
        $this->assign('waitYouAns',$waitYouAns);

    }
   
}















 ?>